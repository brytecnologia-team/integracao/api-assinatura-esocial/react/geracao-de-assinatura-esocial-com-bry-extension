import React, { useEffect, useState } from "react";
import Select from "react-select";
import api from "../../services/api";
import * as utils from "../../utils/utils.js";

export default function Plugin() {

  // *************** Passo 1: ***************

  const [document, setDocument] = useState(null);
  const [installedCertificates, setInstalledCertificates] = useState("");
  const [selectedCertificate, setSelectedCertificate] = useState("");
  const [token, setToken] = useState("");

  // Verifica se a extensão está instalada.
  const extensionInstalled = utils.isExtensionInstalled();
  // Verifica qual navegador está instalado.
  const browser = utils.detectBrowser();

  // ************** Passo 2: ***************

  // Se a extensão está instalada, lista os certificados instalados.
  useEffect(() => {
    if (extensionInstalled) {
      window.BryExtension.listCertificates().then(certificates => {
        certificates.forEach(certificate => {
          certificate.label = certificate.name;
        });
        setInstalledCertificates(certificates);
      });
    }
  }, []);

  function handleSubmit(event) {
    event.preventDefault();
    sign();
  }
  async function sign() {
    const data = new FormData();
    //Conteudo do certificado selecionado
    data.append("certificate", selectedCertificate.certificateData);
    data.append("documento", document)

    // ************** Passo 3: ***************

    try {
      // Requisição para inicialização da assinatura
      const response = await api.post("/initialize", data, {
        headers: { Authorization: token }
      }
);
      console.log(response.data);
      // Cryptografa os dados na extensão
      signExtension(response.data, selectedCertificate.certificateData);
    } catch (err) {
      console.log(err.response);
      alert(err.response);
    }
  }

  // *************** Passo 4: ***************

  async function signExtension(response, certificate) {

    // Altera os valores JSON retornados pela inicialização para um formato aceito pela extensão

    response.formatoDadosEntrada = 'Base64'
    response.formatoDadosSaida = 'Base64'
    response.algoritmoHash = 'SHA256'
    response.assinaturas = response.signedAttributes;
    response.assinaturas[0].hashes = response.assinaturas[0].content;
    response.assinaturas.forEach(
      assinatura => (assinatura.hashes = [assinatura.hashes])
    );

    // Cifra a inicialização da assinatura, passando o ID do certificado e o conteudo da resposta da inicialização da assinatura.
    await window.BryExtension.sign(
      selectedCertificate.certId,
      JSON.stringify(response)
    ).then(async signature => {
      console.log(signature)
      // Utiliza a cifra da assinatura no campo "assinatura" para criar o JSON que será utilizado na finalização da assinatura.
      const data = new FormData()
      data.append('documento', document)
      data.append('initializedDocuments', response.initializedDocuments[0].content)
      data.append('certificate', certificate)
      data.append('cifrado', signature.assinaturas[0].hashes[0])

      // *************** Passo 5: ***************

      // Requisição para finalização da assinatura
      try {
        const response = await api.post("/finalize", data, {
          headers: { Authorization: token }
        }
);
        console.log(response);
      } catch (err) {
        alert(err.response.data.message);
      }
    });
  }
  return (
    <>
      {extensionInstalled ? (
        // Start of the rendered area if the extension is installed **
        <form onSubmit={handleSubmit}>
          <h3>1º Passo: </h3>
          <label htmlFor="certificate">Selecione o certificado que deseja utilizar para assinar *</label>
          <Select
            id="certificate"
            options={installedCertificates}
            onChange={event => setSelectedCertificate(event)}
            value={selectedCertificate}
            required
          />

          {selectedCertificate ? (
            <React.Fragment>
              <input
                type="text"
                readOnly
                value={selectedCertificate.issuer || ""}
              />
              <input
                type="text"
                readOnly
                value={selectedCertificate.expirationDate || ""}
              />
              <input
                type="text"
                readOnly
                value={selectedCertificate.certificateType || ""}
              />
              <textarea
                type="text"
                rows="5"
                value={selectedCertificate.certificateData}
                readOnly
              />
            </React.Fragment>
          ) : (
            ""
          )}
          <label htmlFor="docLabel">Documento a ser assinado *</label>
          <label htmlFor="document" className="fileUp">
            {document ? (
              <React.Fragment>{document.name}</React.Fragment>
            ) : (
              <React.Fragment>
                <i className="fa fa-upload"></i>
                Selecione o arquivo
              </React.Fragment>
            )}
            <input
              id="document"
              type="file"
              required
              onChange={event => setDocument(event.target.files[0])}
            />
          </label>

          <label htmlFor="authToken">AuthToken *</label>
          <input
            id="authToken"
            type="text"
            required
            onChange={event => setToken(event.target.value)}
            placeholder="Token de autenticação BRy"
          />


          <button className="btn" type="submit">
            Assinar
          </button>
        </form>
      ) : (
        // ** End of the rendered area if the extension is installed **

        // ** Start of area renders if extension is NOT installed **
        <div className="container">
          {browser === "chrome" ? (
            <div className="isChrome">
              <h3 className="extension-message">
                Detectamos que a Extensão para Assinatura Digital não está
                instalada.
              </h3>
              <p>Segue abaixo um passo a passo para instalação da extensão</p>
              <p>
                <strong>1º Passo -</strong> Clique no botão abaixo para acessar
                a Extensão no Chrome WebStore
              </p>
              <a
                href="https://chrome.google.com/webstore/detail/dhikfimimcjpoaliefjlffaebdeomeni"
                className="btn btn-lg btn-primary btn-extension-install"
              >
                Instalar Extensão via Chrome WebStore!
              </a>

              <p>
                <strong>2º Passo -</strong> Clique no botão{" "}
                <strong>USAR NO CHROME</strong>
              </p>
              <img
                alt="Print use on chrome button"
                src={require("../../assets/imgs/use_on_chrome_button.jpg")}
              />
              <p>
                <br />
                <br />
                <strong>3º Passo -</strong> Você deve retornar para esta página
                que ela será atualizada.
              </p>
            </div>
          ) : (
            ""
          )}
          {browser === "firefox" ? (
            <div className="isFirefox">
              <h3 className="extension-message">
                Detectamos que a extensão para Assinatura Digital não está
                instalada.
              </h3>

              <p>Clique no botão abaixo para instalar a extensão</p>

              <a
                href="https://www.bry.com.br/downloads/extension/firefox/assinatura_digital_bry.xpi"
                className="btn btn-lg btn-primary btn-extension-install"
              >
                Instalar Extensão!
              </a>
            </div>
          ) : (
            ""
          )}
          {browser === "edge" ? (
            <div className="isEdge">
              <h3 className="extension-message">
                Lamentamos, mas uma versão da extensão só estará disponível para
                o seu navegador na próxima atualização!
              </h3>
            </div>
          ) : (
            ""
          )}
          {browser === "opera" ? (
            <div className="isOpera">
              <h3 className="extension-message">
                Lamentamos, mas uma versão da extensão só estará disponível para
                o seu navegador na próxima atualização!
              </h3>
            </div>
          ) : (
            ""
          )}

          {browser === "safari" ? (
            <div className="isSafari">
              <h3>
                Detectamos que a Extensão para Assinatura Digital não está
                instalanda
              </h3>
              <input
                type="image"
                width="250"
                src={require("../../assets/imgs/baixar.png")}
                alt="Logo apple store"
              />
              <h4>
                Após a instalação é necessário habilitar a extensão nas
                preferências do Safari.
              </h4>
              <p>
                <b>1º Abra o aplicativo "BRy Assinatura Digital" instalado</b>
              </p>
              <p>
                <strong>
                  2º Dentro do aplicativo, clique em habilitar a extensão nas
                  preferências do Safari
                </strong>
              </p>
              <img
                src={require("../../assets/imgs/app.png")}
                alt="Print button extension"
              />
              <p>
                Caso a opção para habilitar a extensão não apareça nas
                preferências do Safari, encerre o navagador, e repita o passo 2.
              </p>
              <p>
                <strong>
                  4º Após ativar a extensão, basta recarregar a página.
                </strong>
              </p>
              <button onClick={document.location.reload(true)}>
                Recarregar página
              </button>
            </div>
          ) : (
            ""
          )}
          {browser === "iE" ? (
            <div className="isIE">
              <div className="extension-message">
                <h3>
                  Lamentamos, mas uma versão da extensão só estará disponível
                  para o seu navegador na próxima atualização!
                </h3>
              </div>
            </div>
          ) : (
            ""
          )}
          {browser === "unknown" ? (
            <div className="isUnknown">
              <h3 className="extension-message">
                Não foi possível identificar o seu browser!
              </h3>
            </div>
          ) : (
            ""
          )}
        </div>
      )
      // ** End of area renders if extension is NOT installed **
      }
    </>
  );
}
